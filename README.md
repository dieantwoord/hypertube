# HyperTube

A Peer to Peer Audio Streaming Server and client using [Hyper-Protocol](https://docs.holepunch.to/)


A high-level API for finding and connecting to peers who are interested in a "topic." 

[@hyperswarm/dht](https://docs.holepunch.to/building-blocks/hyperswarm#dht): 

A DHT powering Hyperswarm. Through this DHT, each server is bound to a unique key pair, with the client connecting to the server using the server's public key.


[@hyperswarm/secretstream](https://docs.holepunch.to/building-blocks/hyperswarm#secretstream): 

Secretstream is used to securely create connections between two peers in Hyperswarm.

## What is HyperSwarm?
Hyperswarm allows you to find and connect to peers who are announcing a common "topic". The swarm topic can be anything. This means that, with Hyperswarm, you can discover and connect peers with a shared interest over a distributed network. As an example, we often use Hypercore's discovery key as the swarm topic for discovering peers to replicate with.
Once Hyperswarm has discovered peers, nodes in the DHT help those peers connect via a UDP-based hole-punching algorithm that's designed to work well in home networks.

https://docs.holepunch.to/building-blocks/hyperswarm

## Usage

To install:

`git clone git@git.codingvm.codes:snxraven/hypertube.git`

`cd hypertube`

`npm i`

To run a new topic (room):

`node hypertube.js`

This will give you a connection topic to share:

```
/hypertube ❯ ✦ node hypertube.js
joined topic: 6076c0903ad293e24c10fceb501fe7b02425f6d26c7a5b2d015abd07e3e6b17b
(Share this key to others so they may join.)
To Play a youtube link, use !play LINKHERE to stop !stop
All commands are global to all peers
```


To connect to an altready made topic (room) pass the hash on start up:


`node hypertube.js 6076c0903ad293e24c10fceb501fe7b02425f6d26c7a5b2d015abd07e3e6b17b`

# Commands

!play [youtube link] - play a youtube link globally to all peers

!stop - Stop all peers audio

Whoever plays the track becomes the broadcaster, all peers will see data coming in in the form of a buffer.

You must !stop the audio before submitting another track.

